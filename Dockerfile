FROM alpine:latest

MAINTAINER Nick Wilburn <now@rincon.com>

RUN apk add --update \
  python-dev \
  py-pip \
  py-setuptools \
  ca-certificates \
  gcc \
  musl-dev \
  linux-headers \
  libffi-dev \
  openssl-dev \
  make \
  curl \
  git \
  && pip install --upgrade --no-cache-dir shade pip "molecule<3" setuptools python-openstackclient tox \
  && apk del gcc musl-dev linux-headers \
  && rm -rf /var/cache/apk/*

RUN curl -o packer.zip https://releases.hashicorp.com/packer/1.5.4/packer_1.5.4_linux_amd64.zip && unzip packer.zip && mv packer /usr/bin/

CMD ["/bin/sh"]
